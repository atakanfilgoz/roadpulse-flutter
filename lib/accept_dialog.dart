import 'package:flutter/material.dart';

import 'show_detected_photos.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class AcceptDialog extends StatefulWidget {
  AcceptDialog();

  @override
  AcceptDialogState createState() => new AcceptDialogState();
}

class AcceptDialogState extends State<AcceptDialog> {
  static bool isDeleted = false;
  String selectedPriority;
  String dropdownValue = '7';
  TextEditingController _controller;


  List<Widget> createRadioListItemsForRoute() {
    _controller = TextEditingController();

    List<Widget> widgets = [];
    /*widgets.add(Container(
      child: Text("Notes"),
    ));
    widgets.add(Scaffold(
      body: Center(
        child: TextField(
          controller: _controller,
          onSubmitted: (String value) async {
            await showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Thanks!'),
                  content: Text('You typed "$value".'),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('OK'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
    ));*/
    
    final List<String> prior = ['DUSUK', 'ORTA', 'YUKSEK', 'IVEDI'];

    for (String element in prior) {
      widgets.add(
        RadioListTile(
          value: element,
          groupValue: selectedPriority,
          title: Text(
            element,
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          onChanged: (currentSelection) {
            setSelectedRadioRouting(currentSelection);
          },
          selected: selectedPriority != null
              ? selectedPriority.compareTo(element) == 0
              : false,
          activeColor: Colors.red[300],
        ),
      );
    }
    return widgets;
  }

  setSelectedRadioRouting(String selected) {
    setState(() {
      if (selectedPriority != null &&
          selected.compareTo(selectedPriority) == 0) {
        selectedPriority = null;
      } else {
        selectedPriority = selected;
      }
    });
  }

  @override
  void dispose() {
    ShowDetectedState.setTimer = false;
    super.dispose();
  }

  _onAcceptPressed(context) async{
    print(selectedPriority);
    ShowDetectedState.setTimer = false;
    final String baseURL = 'https://lit-meadow-91580.herokuapp.com';
    var headers = {'Content-Type': 'application/json'};
    var body = jsonEncode({'id': ShowDetectedState.potholeId.toString(),'label': selectedPriority});
    print(body);
    var response = await http.put(baseURL + '/rest/updatePothole/', body: body, headers: headers);
    int code = response.statusCode;
    if(code == 200){
      print("updatelendi");
    }else{
      print("update basarisiz");
    }
    Navigator.of(context, rootNavigator: true).pop();
  }

  _onCancelPressed(context) {
    //Delete request at.
    ShowDetectedState.setTimer = false;
    Navigator.of(context, rootNavigator: true).pop();
  }



  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      backgroundColor: Colors.white,
      title: Text('Details'),
      content: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: createRadioListItemsForRoute(),
          )),
      actions: <Widget>[
        FlatButton(
            onPressed: () => _onAcceptPressed(context),
            child: Text(
              'OK',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            )),
        FlatButton(
            onPressed: () => _onCancelPressed(context),
            child: Text(
              'Cancecl',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            )),
      ],
    );
  }
}
