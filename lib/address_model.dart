class AddressModel {
  final String country;
  final String street;
  final String city;
  final String district;
  final String street_number;
  final String neighbourhood;
  final String postal_code;

  AddressModel(
      {this.country,
      this.street,
      this.city,
      this.district,
      this.neighbourhood,
      this.street_number,
      this.postal_code});

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      city: json['city'] as String,
      country: json['country'] as String,
      district: json['district'] as String,
      street: json['street'] as String,
      street_number: json['street_number'] as String,
      neighbourhood: json['neighbourhood'] as String,
      postal_code: json['postal_code'] as String,
    );
  }

  @override
  String toString() {
    String ret = "";
    if (neighbourhood != null) {
      ret += neighbourhood + ', ';
    }
    if (street != null) {
      ret += street + ', ';
    }
    if (street_number != null) {
      ret += street_number + ', ';
    }
    if (postal_code != null) {
      ret += postal_code + ', ';
    }
    if (city != null) {
      ret += city + ', ';
    }
    if (district != null) {
      ret += district + ', ';
    }
    if (country != null) {
      ret += country + ', ';
    }
    ret = ret.substring(0,ret.lastIndexOf(', '));

    return ret;
  }
}
