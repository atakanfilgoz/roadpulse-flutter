import 'package:flutter/material.dart';

class BndBox extends StatelessWidget {
  final List<dynamic> results;
  final int previewH;
  final int previewW;
  final double screenH;
  final double screenW;

  BndBox(
      this.results, this.previewH, this.previewW, this.screenH, this.screenW);

  @override
  Widget build(BuildContext context) {
    List<Widget> renderBoxes() {
      if (results == null) return [];
      List<Widget> ret = [];
      var list = results.map((re) {
        return Positioned(
          left: re["rect"]["x"] * previewW,
          top: re["rect"]["y"] * previewH,
          width: re["rect"]["w"] * screenW,
          height: re["rect"]["h"] * screenH,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              border: Border.all(
                color: Colors.red,
                width: 2,
              ),
            ),
            child: Text(
              "${re["detectedClass"]} ${(re["confidenceInClass"] * 100).toStringAsFixed(0)}%",
              style: TextStyle(
                background: Paint()..color = Colors.red,
                color: Colors.white,
                fontSize: 12.0,
              ),
            ),
          ),
        );
      }).toList();
      ret.addAll(list);
      return ret;
    }

    return Stack(children: renderBoxes());
  }
}
