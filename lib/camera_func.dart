import 'dart:math' as math;

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart' as imglib;
import 'package:latlong/latlong.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tflite/tflite.dart';

import 'bndbox.dart';

typedef void Callback(List<dynamic> list, int h, int w);
List<Position> travelledCoords = new List<Position>();

class Camera extends StatefulWidget {
  final List<CameraDescription> cameras;
  Camera(this.cameras);

  @override
  _CameraState createState() => new _CameraState();
}

class _CameraState extends State<Camera> {
  AssetsAudioPlayer assetsAudioPlayer;
  CameraController camera;
  bool _isDetecting = false;
  String res;
  List<dynamic> _recognitions;
  int _imageHeight = 0;
  int _imageWidth = 0;
  Position position;
  Position prevPosition;
  bool _isFirst = true;

  loadModel() async {
    String res;
    res = await Tflite.loadModel(
        model: "assets/model.lite",
        labels: "assets/dict.txt",
        numThreads: 1 // defaults to 1
        );
    print(res);
  }

  List<int> convertImagetoPng(CameraImage image) {
    try {
      imglib.Image img;
      if (image.format.group == ImageFormatGroup.yuv420) {
        img = _convertYUV420(image);
      } else if (image.format.group == ImageFormatGroup.bgra8888) {
        img = _convertBGRA8888(image);
      }

      imglib.PngEncoder pngEncoder = new imglib.PngEncoder();

      // Convert to png
      List<int> png = pngEncoder.encodeImage(img);
      return png;
    } catch (e) {
      print(">>>>>>>>>>>> ERROR:" + e.toString());
    }
    return null;
  }

  imglib.Image _convertBGRA8888(CameraImage image) {
    return imglib.Image.fromBytes(
      image.width,
      image.height,
      image.planes[0].bytes,
      format: imglib.Format.bgra,
    );
  }

  imglib.Image _convertYUV420(CameraImage image) {
    final int width = image.width;
      final int height = image.height;
      final int uvRowStride = image.planes[1].bytesPerRow;
      final int uvPixelStride = image.planes[1].bytesPerPixel;

      print("uvRowStride: " + uvRowStride.toString());
      print("uvPixelStride: " + uvPixelStride.toString());

      // imgLib -> Image package from https://pub.dartlang.org/packages/image
      var img = imglib.Image(width, height); // Create Image buffer

      // Fill image buffer with plane[0] from YUV420_888
      for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
          final int uvIndex =
              uvPixelStride * (x / 2).floor() + uvRowStride * (y / 2).floor();
          final int index = y * width + x;

          final yp = image.planes[0].bytes[index];
          final up = image.planes[1].bytes[uvIndex];
          final vp = image.planes[2].bytes[uvIndex];
          // Calculate pixel color
          int r = (yp + vp * 1436 / 1024 - 179).round().clamp(0, 255);
          int g = (yp - up * 46549 / 131072 + 44 - vp * 93604 / 131072 + 91)
              .round()
              .clamp(0, 255);
          int b = (yp + up * 1814 / 1024 - 227).round().clamp(0, 255);
          // color: 0x FF  FF  FF  FF
          //           A   B   G   R
          img.data[index] = (0xFF << 24) | (b << 16) | (g << 8) | r;
        }
      }

    return img;
  }

  void sendImages(CameraImage img, List<dynamic> recognitions) async {
    getLocation();
    double meters;
    if (_isFirst) {
      meters = 5.1;
      _isFirst = false;
    } else {
      Distance distance = new Distance();
      meters = distance(
          new LatLng(prevPosition.latitude, prevPosition.longitude),
          new LatLng(position.latitude, position.longitude));
    }
    print("Meters " + meters.toString());
    if (meters < 5.0) {
      return;
    }
    prevPosition = position;
    assetsAudioPlayer.open("assets/found.mp3");
    assetsAudioPlayer.play();
    
    String data = "{\"long\": " + position.longitude.toString() + ",\n";
    data += "\"lat\": " + position.latitude.toString() + ",\n";
    data += "\"label\": "+  "\"DUSUK\"" + ",\n";

    data += "\"pixels\": [\n";
    for (int i = 0; i < recognitions.length; i++) {
      data = data + "{\n";
      data = data +
          "\"w\":" +
          recognitions[i]["rect"]["w"].toString() +
          ",\n";
      data = data +
          "\"x\":" +
          recognitions[i]["rect"]["x"].toString() +
          ",\n";
      data = data +
          "\"h\":" +
          recognitions[i]["rect"]["h"].toString() +
          ",\n";
      data = data +
          "\"y\":" +
          recognitions[i]["rect"]["y"].toString() +
          "\n";
      if (recognitions.length - 1 == i) {
        data = data + "}\n]}\n";
      } else {
        data = data + "},\n";
      }
    }
    print(data);
    
    var uri = Uri.parse(
        'https://lit-meadow-91580.herokuapp.com/rest/addPotholeCoord/');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    var request = http.MultipartRequest('POST', uri);
    request.headers.addAll(headers);
    request.fields["pothole"] = data;

    request.files.add(await http.MultipartFile.fromBytes(
        'img_file', convertImagetoPng(img),
        contentType: MediaType('image', 'png'), filename: 'a.png'));
    
    var response = await request.send();
    if (response.statusCode == 200) {
      print('Uploaded!');
    }
      
    else {
      print(response.statusCode);
    }
      
  }

  void getLocation() async {
    if (prevPosition == null) {
      prevPosition = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    }
    position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

    print(position);
  }

  void initState() {
    super.initState();
    assetsAudioPlayer = AssetsAudioPlayer();
    getLocation();
    camera = CameraController(
      widget.cameras[0],
      defaultTargetPlatform == TargetPlatform.iOS
          ? ResolutionPreset.low
          : ResolutionPreset.medium,
    );

    camera.initialize().then((_) {
      loadModel();
      setState(() {});
      camera.startImageStream((CameraImage img) {
        if (!mounted) {
          return;
        }
        if (!_isDetecting) {
          _isDetecting = true;

          int startTime = new DateTime.now().millisecondsSinceEpoch;
          Tflite.detectObjectOnFrame(
                  bytesList: img.planes.map((plane) {
                    return plane.bytes;
                  }).toList(), // required
                  model: "YOLO",
                  imageHeight: img.height,
                  imageWidth: img.width,
                  imageMean: 0, // defaults to 127.5
                  imageStd: 255.0, // defaults to 127.5// defaults to 5
                  threshold: 0.2, // defaults to 0.1
                  numResultsPerClass: 5, // defaults to 5
                  blockSize: 32, // defaults to 32
                  numBoxesPerBlock: 5, // defaults to 5
                  asynch: true // defaults to true
                  )
              .then((recognitions) {
            if (recognitions.length != 0) {
              sendImages(img, recognitions);
            }
            int endTime = new DateTime.now().millisecondsSinceEpoch;
            travelledLocations();
            print("Detection took ${endTime - startTime}");
            print(recognitions);
            setRecognitions(recognitions, img.height, img.width);
            _isDetecting = false;
          });
        }
      });
    });
  }

  @override
  void dispose() {
    sendCoords();
    camera?.dispose();
    super.dispose();
  }

  void sendCoords() async {
    String baseURL = "https://lit-meadow-91580.herokuapp.com/rest/addRoute/";
    String data = "{\"coordinates\": [\n";
    print("Travelled Loc Length");
    print(travelledCoords.length);
    for (int i = 0; i < travelledCoords.length;i++) {
      print(travelledCoords[i].latitude.toString() + " "  + travelledCoords[i].longitude.toString());
      print("---");
    }
    if (travelledCoords.length < 50) { // for short trips or tests
      return;
    }
    for (int i = 0; i < travelledCoords.length; i++) {
      data = data + "{\n";
      data = data +
          "\"latitude\":" +
          travelledCoords[i].latitude.toString() +
          ",\n";
      data = data +
          "\"longitude\":" +
          travelledCoords[i].longitude.toString() +
          "\n";
      if (travelledCoords.length - 1 == i) {
        data = data + "}\n]}\n";
      } else {
        data = data + "},\n";
      }
    }
    http.Response response = await http.post(baseURL, body: data);
    travelledCoords = new List<Position>();
    print("Travelled Coords");
    print(data);
    print(response.statusCode);
  }

  setRecognitions(recognitions, imageHeight, imageWidth) {
    if (this.mounted) {
      setState(() {
        _recognitions = recognitions;
        _imageHeight = imageHeight;
        _imageWidth = imageWidth;
      });
    }
  }

  void travelledLocations() async {
    Position prevLoc = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    if (travelledCoords.length == 0) {
      travelledCoords.add(prevLoc);
    }
    else {
      if (travelledCoords[travelledCoords.length - 1].latitude.toString().substring(0,7) !=
              prevLoc.latitude.toString().substring(0,7) ||
          travelledCoords[travelledCoords.length - 1].longitude.toString().substring(0,7) !=
              prevLoc.longitude.toString().substring(0,7)) {
        travelledCoords.add(prevLoc);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          (!camera.value.isInitialized) ? new Container() : buildCameraView(),
        ],
      ),
    );
  }

  Widget buildCameraView() {
    if (camera == null || !camera.value.isInitialized) {
      return Container();
    }

    var tmp = MediaQuery.of(context).size;
    var screenH = math.max(tmp.height, tmp.width);
    var screenW = math.min(tmp.height, tmp.width);
    tmp = camera.value.previewSize;
    var previewH = math.max(tmp.height, tmp.width);
    var previewW = math.min(tmp.height, tmp.width);
    var screenRatio = screenH / screenW;
    var previewRatio = previewH / previewW;

    return Stack(
      children: [
        OverflowBox(
          maxHeight: screenRatio > previewRatio
              ? screenH
              : screenW / previewW * previewH,
          maxWidth: screenRatio > previewRatio
              ? screenH / previewH * previewW
              : screenW,
          child: CameraPreview(camera),
        ),
        BndBox(
          _recognitions == null ? [] : _recognitions,
          math.max(_imageHeight, _imageWidth),
          math.min(_imageHeight, _imageWidth),
          tmp.height,
          tmp.width,
        ),
      ],
    );
  }
}
