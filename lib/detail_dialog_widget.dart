import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:roadpulse_flutter/marker_model.dart';
import 'package:roadpulse_flutter/report_data_model.dart';
import 'package:roadpulse_flutter/report_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'manuel_photo.dart';

class DetailDialogWidget extends StatelessWidget {
  DetailDialogWidget({@required this.model, this.cameras, this.isAdmin});
  final List<CameraDescription> cameras;
  final MarkerModel model;
  final bool isAdmin; 

    final Map<String, Color> colorOfImportanceSet = {
      'IVEDI': Colors.red,
      'YUKSEK': Colors.orange,
      'ORTA': Colors.yellow[600],
      'DUSUK': Colors.green
    };

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width / 2,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black54,
                        offset: Offset(0.0, 4.0),
                        blurRadius: 10.0),
                  ]),
              child: Container(
                  margin: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white70,
                  ),
                  child: GestureDetector(
                      onTap: () => _biggerPhoto(context),
                      child: Container(
                          margin: EdgeInsets.all(10.0),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width / 2,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: NetworkImage(model.link),
                                fit: BoxFit.fill),
                          )))),
        )),
            Expanded(
                child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black54,
                        offset: Offset(0.0, 4.0),
                        blurRadius: 10.0),
                  ]),
              child: Container(
                padding: EdgeInsets.all(10.0),
                margin: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white70,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      model.label,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: colorOfImportanceSet[model.label]),
                    ),
                     SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.black45,
                      ),
                      child: Text(
                        model.formatCreationDate(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 19.0,
                          color: Colors.white70,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      model.addr.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 17.0, fontWeight: FontWeight.w300),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    !isAdmin ? RaisedButton(
                        child: Text("Give Feedback"),
                        shape: StadiumBorder(),
                        color: Colors.white70,
                        onPressed: _isFeedBackOnCurrentReport(model.id) ? null : ()  async {
                          _onFeedBack(context);
                        }) : Container()
                  ],
                ),
              ),
            )),
          ],
        ),
      ],
    );
  }

  _onFeedBack(context) async {
    print(model.id);
    await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => new TakePictureScreen(
              cameras,
              false,
              model: model,
            )));
  }
  
  _isFeedBackOnCurrentReport(potholeId) {
    if(ReportPage.reportList != null){
      for(ReportData reportData in ReportPage.reportList ){
        if(reportData.model.id == potholeId){
          return true;
        }
      }
    }
    return false;
  }

  _biggerPhoto(context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext contex) {
          return Dialog(
              child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width * 2,
            child: ClipRect(
              child: PhotoView(
                tightMode: true,
                imageProvider: NetworkImage(model.link),
                maxScale: PhotoViewComputedScale.covered * 2.0,
                minScale: PhotoViewComputedScale.contained * 1,
                initialScale: PhotoViewComputedScale.contained,
              ),
            ),
          ));
        });
  }
}
