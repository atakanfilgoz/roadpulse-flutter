import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

class EditDialog extends StatefulWidget {
  String selectedElement; //Bu alan selected ivedilik düzeyi
  final int id; //Bu alan MarkerModel id'si
  final Function(int,String) notifyParent;


  EditDialog({Key key,@required this.selectedElement, @required this.id, @required this.notifyParent}): super(key: key);

  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<EditDialog> {
  final String baseURL = 'https://lit-meadow-91580.herokuapp.com';
  final List<String> prior = ['IVEDI', 'YUKSEK', 'ORTA', 'DUSUK'];
  Map<String, Color> colorOfImportanceSet = {
    'IVEDI': Colors.red,
    'YUKSEK': Colors.orange,
    'ORTA': Colors.yellow[600],
    'DUSUK': Colors.green
  };

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      backgroundColor: Colors.white,
      title: Text('Edit the importance level of the selected pothole'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: createRadioListItems(),
      ),
      actions: <Widget>[
        FlatButton(
            onPressed: () => _onEditPressed(context),
            child: Text(
              'Edit',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            )),
        FlatButton(
            onPressed: () => _onCancelPressed(context),
            child: Text(
              'Cancel',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            )),
      ],
    );
  }

  List<Widget> createRadioListItems() {
    List<Widget> widgets = [];
    for (String element in prior) {
      widgets.add(
        RadioListTile(
          value: element,
          groupValue: widget.selectedElement,
          title: Text(
            element,
            style: TextStyle(
              color: colorOfImportanceSet[element],
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          onChanged: (currentSelection) {
            setSelectedRadio(currentSelection);
          },
          selected: widget.selectedElement.compareTo(element) == 0,
          activeColor: colorOfImportanceSet[element],
        ),
      );
    }
    return widgets;
  }

  setSelectedRadio(String selected) {
    setState(() {
      widget.selectedElement = selected;
    });
  }

  _onCancelPressed(context) {
    Navigator.pop(context);
  }

  Future _onEditPressed(context) async {
    var headers = {'Content-Type': 'application/json'};
    var body = jsonEncode({'id': widget.id.toString(),'label': widget.selectedElement.toString()});
    print(body);
    var response = await put(baseURL + '/rest/updatePothole/', body: body, headers: headers);    
    int code = response.statusCode;
    if(code == 200){
      Navigator.pop(context);
      widget.notifyParent(widget.id, widget.selectedElement);
    }    
  }
}
