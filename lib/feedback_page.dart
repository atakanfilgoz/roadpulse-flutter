import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:roadpulse_flutter/marker_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class FeedbackPage extends StatefulWidget {
  final String imagePath;
   final MarkerModel model;

  const FeedbackPage({Key key, this.imagePath, this.model}) : super(key: key);

  _FeedbackPage createState() => _FeedbackPage();
}

class _FeedbackPage extends State<FeedbackPage> {
  PageController _controller;
  TextEditingController noteController;
  TextEditingController titleController;

  @override
  void initState() {
    _controller = PageController(initialPage: 0, viewportFraction: 0.9);
    noteController = new TextEditingController();
    titleController = new TextEditingController();
   
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Give Feedback'),
        backgroundColor: Colors.orange[200],
      ),
      body: PageView(
          scrollDirection: Axis.vertical,
          controller: _controller,
          children: [imageWidget(), noteWidget()]),
    );
  }

  Widget imageWidget() {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 20.0, left: 10.0, right: 10.0),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0),
          color: Colors.brown[100],
          boxShadow: [
            BoxShadow(
                color: Colors.black87,
                offset: Offset(0.0, 4.0),
                blurRadius: 10.0),
          ]),
      child: Image.file(File(widget.imagePath)),
    );
  }

  Widget noteWidget() {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Column(children: [
          Text("Add Title To Your Feedback",
              style: TextStyle(
                fontFamily: "Poppins",
                fontSize: 20,
                fontWeight: FontWeight.w900,
              )),
          Container(
              margin: EdgeInsets.all(20.0),
              child: TextField(
                  controller: titleController,
                  textInputAction: TextInputAction.done,
                  decoration: new InputDecoration(
                    hintText: "Enter a Title",
                    labelText: "Title",
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                  ),
                  keyboardType: TextInputType.text,
                  maxLines: 1,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ))),
          Text("Add Note To Your Feedback",
              style: TextStyle(
                fontFamily: "Poppins",
                fontSize: 20,
                fontWeight: FontWeight.w900,
              )),
          Container(
              margin: EdgeInsets.all(20.0),
              child: TextField(
                  controller: noteController,
                  textInputAction: TextInputAction.done,
                  decoration: new InputDecoration(
                    hintText: "Enter a Note",
                    labelText: "Note",
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                  ),
                  keyboardType: TextInputType.multiline,
                  maxLines: 10,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ))),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              RaisedButton(
                onPressed: _onAcceptPressed,
                child: Text("Accept", style: TextStyle(color: Colors.white)),
                color: Colors.lightGreen[800],
                shape: const StadiumBorder(),
              ),
              SizedBox(
                width: 15.0,
              ),
              RaisedButton(
                onPressed: _onClearPressed,
                color: Colors.redAccent[700],
                child: Text("Clear", style: TextStyle(color: Colors.white)),
                shape: const StadiumBorder(),
              )
            ],
          )
        ]));
  }

  _onAcceptPressed() async {
    String noteText = noteController.text;
    print(noteText);
     
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    String title = titleController.text;
    String note = noteController.text;

    Map reportData = {"title": title, "text": note};
    List imageMapping = new List();

    for (int i = 0; i < 1; i++) {
      int x = i + 1;
      List data = ["img_file" + x.toString()];
      Map pothole = {"id": widget.model.id, "images": data};
      imageMapping.add(pothole);
    }
    print(imageMapping.toString());

    var uri =
        Uri.parse('https://lit-meadow-91580.herokuapp.com/rest/sendReport/');
    var request = http.MultipartRequest('POST', uri);
    request.headers.addAll(headers);
    print(request.fields["report"]);

    for (int i = 0; i < 1; i++) {
      int x = i + 1;

      request.files.add(new http.MultipartFile.fromBytes(
          'img_file' + x.toString(),
          await File.fromUri(Uri.parse(widget.imagePath))
              .readAsBytes(),
          contentType: MediaType('image', 'png'),
          filename: 'a.png'));
    }
    request.fields["report"] = json.encode(reportData);
    request.fields["image_mapping"] = json.encode(imageMapping);
    var response = await request.send();

    if (response.statusCode == 200) {
      Navigator.of(context).pop(reportData);
      //ReportPage.reportList = null;
      print('Uploaded!');
    } else {
      print(response.request.headers.toString());
      print(response.statusCode);
    }

    /*request.files.add(await http.MultipartFile.fromBytes(
        'img_file', convertImagetoPng(img),
        contentType: MediaType('image', 'png'), filename: 'a.png'));

    var response = await request.send();
    if (response.statusCode == 200) {
      print('Uploaded!');
    }*/
  }

  _onClearPressed() {
    noteController.clear();
  }
}
/*validator: (val) {
                        if(val.length==0) {
                          return "Email cannot be empty";
                        }else{
                          return null;
                        }
                      },*/
