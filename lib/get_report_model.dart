import 'package:roadpulse_flutter/image_model.dart';

class GetReportModel {
  final int report_id;
  final String creation_date;
  final String text;
  final String title;
  final int sender_id;
  final String status;
  final String reply_text;
  final List<FixedPotholes> fixed_potholes;
  final List<int> validated_potholes;
  final List<int> denied_potholes;

  GetReportModel(
      {this.report_id,
      this.creation_date,
      this.text,
      this.title,
      this.sender_id,
      this.fixed_potholes,
      this.denied_potholes,
      this.status,
      this.reply_text,
      this.validated_potholes});

  factory GetReportModel.fromJson(Map<String, dynamic> json) {
    var list = json['fixed_potholes'] as List;
    List<FixedPotholes> fixed_potholes =
        list.map((i) => FixedPotholes.fromJson(i)).toList();

    var validatedList = json['validated_potholes'] as List;
    List<int> validatedIntList = new List<int>.from(validatedList);
    var deniedList = json['denied_potholes'] as List;
    List<int> deniedIntList = new List<int>.from(deniedList);

    return GetReportModel(
        creation_date: json['creation_date'] as String,
        report_id: json['id'] as int,
        text: json['text'] as String,
        title: json['title'] as String,
        status: json['status'] as String,
        reply_text: json['reply_text'] as String,
        sender_id: json['sender_id'] as int,
        fixed_potholes: fixed_potholes,
        validated_potholes: validatedIntList,
        denied_potholes: deniedIntList);
  }

  String onlyDate() {
    if (creation_date != null) {
      List<String> parsedDate = creation_date.split("-");
      return parsedDate[2].substring(0, parsedDate[2].indexOf("T")) +
          "/" +
          parsedDate[1] +
          "/" +
          parsedDate[0];
    }
    return "";
  }
}

class FixedPotholes {
  final int pothole_id;
  final List<ImageModel> images;

  FixedPotholes({this.pothole_id, this.images});

  factory FixedPotholes.fromJson(Map<String, dynamic> json) {
    var list = json['images'] as List;
    List<ImageModel> images = list.map((i) => ImageModel.fromJson(i)).toList();

    return FixedPotholes(pothole_id: json['pothole_id'] as int, images: images);
  }
}
