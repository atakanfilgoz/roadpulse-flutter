import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:roadpulse_flutter/get_report_single_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'get_report_model.dart';

class GetReportPage extends StatefulWidget {
  List<GetReportModel> reportModels = new List();

  _GetReportPage createState() => _GetReportPage();
}

class _GetReportPage extends State<GetReportPage> {
  final String baseURL = 'https://lit-meadow-91580.herokuapp.com';
  @override
  void initState() {
    super.initState();
    getReport();
  }

  String element = "";
  Future getReport() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    print("sessionId: " + sessionId);
    var body = jsonEncode({});

    Map<String, String> headers = {"cookie": sessionId};
    var response =
        await post(baseURL + '/rest/getReports/', headers: headers, body: body);
    var extractedData = jsonDecode(response.body);
    if (response.statusCode == 200) {
      print(extractedData.toString());
      for (var element in extractedData) {
        GetReportModel model = new GetReportModel.fromJson(element);
        widget.reportModels.add(model);
      }
      /*for (var element in widget.reportModels) {
        print(element.fixed_potholes[1].images[0].link);
      }*/
      print(widget.reportModels.length);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(20),
            ),
          ),
          backgroundColor: Colors.orange[200],
          title: Text("Reports"),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Stack(
          children: <Widget>[
            ListView.builder(
              itemCount: widget.reportModels.length,
              itemBuilder: (context, index) {
                return Card(
                    color: Colors.black38,
                    child: ListTile(
                        onTap: () => Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) => GetReportSingle(
                                    reportModel: widget.reportModels[index]))),
                        title: Text(widget.reportModels[index].title.toString(),
                            style: TextStyle(color: Colors.black87)),
                        leading: Image.network(widget.reportModels[index]
                            .fixed_potholes[0].images[0].link),
                        subtitle: subTitleWidget(index)));
              },
            )
          ],
        ));
  }

  Widget subTitleWidget(index) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(widget.reportModels[index].text.toString(),
              style: TextStyle(color: Colors.white)),
           Text(widget.reportModels[index].reply_text != null ? "Reply Text:" : "",
              style: TextStyle(
                  color: Colors.white60, decoration: TextDecoration.underline)),
          Text(widget.reportModels[index].reply_text != null ? widget.reportModels[index].reply_text.toString(): "",
              style: TextStyle(color:Colors.white)),
          Text("Report Status:",
              style: TextStyle(
                  color: Colors.white60, decoration: TextDecoration.underline)),
           Text(widget.reportModels[index].status.toString(),
              style: TextStyle(color: widget.reportModels[index].status.toString() == "DENIED"?Colors.redAccent[400]: Colors.green))
        ]);
  }
}
