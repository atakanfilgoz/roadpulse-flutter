import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:photo_view/photo_view.dart';
import 'package:roadpulse_flutter/send_feedback_answer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'get_report_model.dart';
import 'marker_model.dart';

class GetReportSingle extends StatefulWidget {
  final GetReportModel reportModel;

  GetReportSingle({Key key, this.reportModel}) : super(key: key);

  _GetReportSingle createState() => _GetReportSingle();
}

class _GetReportSingle extends State<GetReportSingle> {
  Map<int, bool> potholeMapping;
  String baseURL = "https://lit-meadow-91580.herokuapp.com/rest";
  Map<int, MarkerModel> oldPotholes;
  SharedPreferences prefs;
  bool isAdmin = false;

  @override
  void initState() {
    potholeMapping = new Map();
    oldPotholes = new Map();
    shared();
    for (var element in widget.reportModel.fixed_potholes) {
      _getOldInformationOfPothole(element.pothole_id);
    }
    print(widget.reportModel.denied_potholes.length);
    for (var element in widget.reportModel.fixed_potholes) {
      potholeMapping[element.pothole_id] = false;
      for (int element1 in widget.reportModel.validated_potholes) {
        if (element.pothole_id == element1) {
          potholeMapping[element.pothole_id] = true;
        }
      }
    }
  }

  void shared() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      isAdmin = prefs.getBool("isAdmin");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(20),
          ),
        ),
        backgroundColor: Colors.orange[200],
        title: Text("Details of Report"),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Stack(children: [
        SingleChildScrollView(child: Column(children: _allReport(context)))
      ]),
      floatingActionButton: isAdmin ? FloatingActionButton(
      backgroundColor: Colors.transparent,
      child: Icon(Icons.check),
       onPressed: widget.reportModel.status.toString() != "NOTSEEN" ? null : () async {
            await _showDialog(context);
            Navigator.of(context).pop();
          }
      ): null,
    );
  }
    _showDialog(context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext contex) {
          return SendFeedbackAnswer(potholeMapping: potholeMapping, report_id: widget.reportModel.report_id,);
        });
  }

  _sendResultFromFeedback() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    List<int> fixedOnes = new List();
    List<int> notFixedOnes = new List();
    String status = "PASSED";

    for (var element in potholeMapping.keys) {
      if (potholeMapping[element] == false) {
        status = "DENIED";
        notFixedOnes.add(element);
      } else {
        fixedOnes.add(element);
      }
    }

    var body = jsonEncode({
      "text": "Deneme",
      "reply_status": status,
      "validated_pothole_ids": fixedOnes,
      "denied_pothole_ids": notFixedOnes,
      "replied_report_id": widget.reportModel.report_id
    });
    print(body);
    var response =
        await post(baseURL + '/replyReport/', headers: headers, body: body);
    var extractedData = jsonDecode(response.body);
    if (response.statusCode == 200) {
      print(extractedData.toString());
    }
  }

  List<Widget> _allReport(context) {
    List<Widget> widgets = [];

    widgets.add(cardWidget());
    int i = 1;
    for (var element in widget.reportModel.fixed_potholes) {
      widgets.add(imageWidget(Image.network(element.images[0].link), context,
          NetworkImage(element.images[0].link), i, element.pothole_id));

      i++;
    }
    return widgets;
  }

  Widget potholeNumber(text) {
    return Container(
        padding: EdgeInsets.all(5.0),
        child: Text(text,
            style: TextStyle(
                fontFamily: "Poppins",
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.w900,
                decoration: TextDecoration.underline)));
  }

  Widget cardWidget() {
    return Card(
        margin: EdgeInsets.all(10),
        color: Colors.black38,
        child: ListTile(title: titleWidget(), subtitle: subTitleWidget()));
  }

  Widget titleWidget() {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text("Report Title:",
              style: TextStyle(
                color: Colors.white60,
                decoration: TextDecoration.underline,
              )),
          Text(widget.reportModel.title, style: TextStyle(color: Colors.white))
        ]);
  }

  Widget subTitleWidget() {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text("Report Send Date:",
              style: TextStyle(
                  color: Colors.white60, decoration: TextDecoration.underline)),
          Text(widget.reportModel.onlyDate(),
              style: TextStyle(color: Colors.white)),
          Text("Report Note:",
              style: TextStyle(
                  color: Colors.white60, decoration: TextDecoration.underline)),
          Text(widget.reportModel.text.toString(),
              style: TextStyle(color: Colors.white))
        ]);
  }

  Widget imageWidget(childImage, context, providers, i, potholeId) {
    return Card(
        margin: EdgeInsets.all(10),
        color: Colors.black38,
        child: Column(children: [
          GestureDetector(
              onTap: () => _biggerPhoto(context, providers),
              child: Column(children: <Widget>[
                potholeNumber("New Photo of Pothole $i"),
                Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.brown[100],
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black87,
                              offset: Offset(0.0, 4.0),
                              blurRadius: 10.0),
                        ]),
                    child: childImage),
              ])),
          GestureDetector(
              onTap: () => _biggerPhoto(
                  context, NetworkImage(oldPotholes[potholeId].link)),
              child: Column(children: <Widget>[
                potholeNumber("Old Photo of Pothole $i"),
                Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.brown[100],
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black87,
                              offset: Offset(0.0, 4.0),
                              blurRadius: 10.0),
                        ]),
                    child: oldPotholes[potholeId] != null
                        ? Image.network(oldPotholes[potholeId].link)
                        : Container()),
              ])),
          _checkBox(potholeId)
        ]));
  }

  Widget _checkBox(potholeId) {
    return CheckboxListTile(
      value: potholeMapping[potholeId],

      onChanged: !isAdmin || widget.reportModel.status.toString() != "NOTSEEN" 
          ? null
          : (bool val) {
              setState(() {
                potholeMapping[potholeId] = val;
                print(potholeMapping[potholeId]);
              });
            },
      activeColor: Colors.green,
      title: Text(
        "Is Pothole Fixed?",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800),
      ),
      subtitle: Text("If it is fixed please check the box.",
          style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w400)),
    );
  }

  Future _getOldInformationOfPothole(potholeId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    var body = jsonEncode({"id": potholeId});
    var response = await post(baseURL + '/rest/getPotholes/',
        headers: headers, body: body);
    var extractedData = jsonDecode(response.body);
    for (var element in extractedData) {
      setState(() {
        oldPotholes[potholeId] = new MarkerModel.fromJson(element);
      });
    }
    //print(oldPotholes[potholeId].id);
  }

  _biggerPhoto(context, imageWithProvider) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return Dialog(
              child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width * 2,
            child: ClipRect(
              child: PhotoView(
                tightMode: true,
                imageProvider: imageWithProvider,
                maxScale: PhotoViewComputedScale.covered * 2.0,
                minScale: PhotoViewComputedScale.contained * 1,
                initialScale: PhotoViewComputedScale.contained,
              ),
            ),
          ));
        });
  }
}
