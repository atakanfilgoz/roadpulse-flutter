import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:roadpulse_flutter/marker_model.dart';
import 'package:roadpulse_flutter/report_data_model.dart';

import 'feedback_page.dart';
import 'report_screen.dart';


class FeedbackScreen extends StatelessWidget {
  final String imagePath;
  final MarkerModel model;

  const FeedbackScreen({Key key, this.imagePath, this.model}) : super(key: key);

  _goToFeedBackPage(context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FeedbackPage(imagePath: imagePath, model: model),
      ),
    );
  }
  _addCurrentReport(context){
    ReportData reportData = new ReportData(model: this.model, imagePath: this.imagePath) ;
    if(ReportPage.reportList == null){
      ReportPage.reportList = new List();
    }
    ReportPage.reportList.add(reportData);
    Navigator.of(context).pop(reportData);
  }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Give Feedback'),
          backgroundColor: Colors.orange[200],
        ),
        body: Image.file(File(imagePath)),
        floatingActionButton: SpeedDial(
          backgroundColor: Colors.orange[400],
           animatedIcon: AnimatedIcons.list_view,
           animatedIconTheme: IconThemeData(size: 22.0),
           
          children: [
          SpeedDialChild(
            label: "Send Solo",
            backgroundColor: Colors.orange[400],
            child: Icon(Icons.done),
            onTap: () => _goToFeedBackPage(context),
          ),
          SpeedDialChild(
            label: "Add to Current Report",
            backgroundColor: Colors.orange[400],
            child: Icon(Icons.add_a_photo),
            onTap: () => _addCurrentReport(context),
          ),
        ]));
    }
  }

