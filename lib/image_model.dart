
class ImageModel{

  final String link;

  ImageModel({
    this.link
  });

  factory ImageModel.fromJson(Map<String, dynamic> json) {
    return ImageModel(link: json['link'] as String);
  }

  String getLink(){
    return link;
  }
}