import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:roadpulse_flutter/marker_model.dart';

import 'give_feedback.dart';

String title = "";
String message = "";

class TakePictureScreen extends StatefulWidget {
  final List<CameraDescription> cameras;
  final MarkerModel model ;
  final bool isManuelDetection;
  TakePictureScreen(this.cameras, this.isManuelDetection, {this.model});

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.cameras[0],
      ResolutionPreset.high,
    );
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Take a picture')),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        onPressed: () async {
          try {
            await _initializeControllerFuture;

            final path = join(
              (await getTemporaryDirectory()).path,
              '${DateTime.now()}.png',
            );
            await _controller.takePicture(path);
            if (widget.isManuelDetection) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DisplayPictureScreen(imagePath: path),
                ),
              );
            } else {
              final reportData = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FeedbackScreen(
                      imagePath: path, model: widget.model),
                ),
              );
              if(reportData != null){
                Navigator.of(context).pop(reportData);
              }

            }
          } catch (e) {
            print(e);
          }
        },
      ),
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  showAlertDialog(BuildContext context) {
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<bool> _uploadPhoto() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    Map potholeData = {
      "long": position.longitude,
      "lat": position.latitude,
      "label": "DUSUK"
    };

    var uri = Uri.parse(
        'https://lit-meadow-91580.herokuapp.com/rest/addPotholeCoord/');
    var request = http.MultipartRequest('POST', uri);
    request.fields["pothole"] = json.encode(potholeData);

    request.files.add(await http.MultipartFile.fromPath('img_file', imagePath,
        contentType: MediaType('image', 'png'), filename: 'a.png'));

    var response = await request.send();
    if (response.statusCode == 200) {
      title = "Successful";
      message = "Photo is uploaded!";
    } else {
      title = "Warning";
      message = "Photo is not uploaded! Try again.";
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Pothole')),
      body: Image.file(File(imagePath)),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.cloud_upload),
        onPressed: () {
          _uploadPhoto().then((booleanValue) {
            showAlertDialog(context);
          });
        },
      ),
    );
  }
}
