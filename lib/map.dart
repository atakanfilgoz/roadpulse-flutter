import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:http/http.dart';
import 'package:roadpulse_flutter/camera_func.dart';
import 'package:roadpulse_flutter/detail_dialog_widget.dart';
import 'package:roadpulse_flutter/login_screen.dart';
import 'package:roadpulse_flutter/main.dart';
import 'package:roadpulse_flutter/manuel_photo.dart';
import 'package:roadpulse_flutter/map_routing.dart';
import 'package:roadpulse_flutter/marker_model.dart';
import 'dart:async';
import 'package:camera/camera.dart';
import 'package:roadpulse_flutter/messaing_firebase.dart';
import 'package:roadpulse_flutter/pothole_list_view.dart';
import 'package:roadpulse_flutter/report_screen.dart';
import 'package:roadpulse_flutter/show_detected_photos.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'edit_dialog_widget.dart';
import 'package:google_map_polyline/google_map_polyline.dart';

import 'get_report_page.dart';

List<CameraDescription> cameras;
const kGoogleApiKey = "AIzaSyDAZZsGXcxIYgC17UBiyT37JC k8fDENChI";
GoogleMapPolyline googleMapPolyline =
    new GoogleMapPolyline(apiKey: "AIzaSyAC8MIBdlw4lUCEX-MhscJflN0VZR6lOK0");

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class MyMap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'roadpulse',
      home: MapSample(),
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();
  Map<int, int> markerPageMap =
      new Map(); //MarkerId ile pagenumber arasındaki ilişki. TODO: DELETE gelince duzenleme yapılacak.

  PageController _pageController;
  final Map<String, Color> colorOfImportanceSet = {
    'IVEDI': Colors.red,
    'YUKSEK': Colors.orange,
    'ORTA': Colors.yellow[600],
    'DUSUK': Colors.green
  };
  Position position;
  static LatLng _center; //= const LatLng(39.925018, 32.836956);
  final List<Marker> _markers = [];
  LatLng _lastMapPosition = _center;
  MapType _currentMapType = MapType.normal;
  String searchAddr;
  Set<Polyline> polyline = new Set<Polyline>();
  List<LatLng> routeCoords = new List<LatLng>();
  SharedPreferences prefs;
  bool isAdmin = false;
  int routeId = 1;

  final String baseURL = 'https://lit-meadow-91580.herokuapp.com';
  List<MarkerModel> markerModels = new List();

  void cameraInitializer() async {
    cameras = await availableCameras();
  }

  void shared() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      isAdmin = prefs.getBool("isAdmin");
    });
  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
    shared();
    cameraInitializer();
    getLocation();
    getPotholes();
    showTravelledRoads();
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8);
  }

  /*Future<String> _getId() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (Theme.of(context).platform == TargetPlatform.iOS) {
    IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.identifierForVendor; // unique ID on iOS
  } else {
    AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.androidId; // unique ID on Android
  }

  String deviceId = await DeviceId.getID;
    //print("DEVICE: !!!!!!!!!!!!!!!!!!!!!!        " + deviceId);
}*/

  void getLocation() async {
    position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    setState(() {
      _center = LatLng(position.latitude, position.longitude);
    });
    print(position);
  }

  _onMapCreated(GoogleMapController controller) {
    setState(() {
      _controller.complete(controller);
    });
  }

  _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  Future<void> moveCamera() async {
    final GoogleMapController controller = await _controller.future;
    MarkerModel realtedMarker;
    for (var item in markerModels) {
      if (item.id == markerPageMap[_pageController.page.toInt()]) {
        realtedMarker = item;
      }
    }
    LatLng target = new LatLng(realtedMarker.latitude, realtedMarker.longitude);
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: target, zoom: 20.0, bearing: 45.0, tilt: 45.0)));
  }

  _onMapTypeButtonPressed() {
    setState(() {
      if (_currentMapType == MapType.normal) {
        _currentMapType = MapType.satellite;
      } else {
        _currentMapType = MapType.normal;
      }
    });
  }

  _onCameraButtonPressed() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => new Camera(cameras)));
  }

  _onManuelDetection() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => new TakePictureScreen(cameras, true)));
  }

  _onControl() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => new ShowDetected()));
  }

  _onRoutingButtonPressed() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => new MyRoutingMap()));
  }

  _onLogoutButtonPressed(context) async {
    var response = await get(baseURL + '/rest/api_logout/');
    var extractedData = jsonDecode(response.body);
    print(extractedData.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('sessionId');
    element = null;
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => new MyApp()));
  }

  _markersList(index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 125.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: InkWell(
        onTap: () {
          moveCamera();
        },
        child: Stack(
          children: [
            Container(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                height: 125.0,
                width: 275.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(
                      color: Colors.black,
                      width: 0.6,
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black54,
                          offset: Offset(0.0, 4.0),
                          blurRadius: 10.0),
                    ]),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () => detail(context, index),
                        child: Container(
                          height: 90.0,
                          width: 90.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                topLeft: Radius.circular(10.0)),
                            image: DecorationImage(
                                image: NetworkImage(markerModels[index].link),
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                      SizedBox(width: 5.0),
                      Flexible(
                        //Belki silinebilir. piksel taşması sorununun kesinliği için eklendi.
                        fit: FlexFit.tight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              markerModels[index].label,
                              style: TextStyle(
                                  fontSize: 11.5,
                                  fontWeight: FontWeight.bold,
                                  color: colorOfImportanceSet[
                                      markerModels[index].label]),
                            ),
                            Text(
                              markerModels[index].formatCreationDate(),
                              style: TextStyle(
                                  fontSize: 11.0, fontWeight: FontWeight.w600),
                            ),
                            Container(
                              width: 170.0,
                              child: Text(
                                markerModels[index].addr.toString(),
                                style: TextStyle(
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            isAdmin
                                ? Flexible(
                                    fit: FlexFit.loose,
                                    child: Padding(
                                        padding: EdgeInsets.only(bottom: 5.0),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              width: 75.0,
                                              height: 20.0,
                                              child: RaisedButton(
                                                onPressed: () {
                                                  showDialog(
                                                      context: context,
                                                      builder: (_) {
                                                        return EditDialog(
                                                            selectedElement:
                                                                markerModels[
                                                                        index]
                                                                    .label,
                                                            id: markerModels[
                                                                    index]
                                                                .id,
                                                            notifyParent:
                                                                editReturned);
                                                      });
                                                },
                                                //shape: const StadiumBorder(),
                                                child: Text(
                                                  'Edit',
                                                  style: TextStyle(
                                                      fontSize: 8.0,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  maxLines: 1,
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: 10.0),
                                            SizedBox(
                                              width: 75.0,
                                              height: 20.0,
                                              child: RaisedButton(
                                                onPressed: () => confirm(
                                                    context,
                                                    'Are you sure you want to remove this pothole?',
                                                    'Caution! If you delete this pothole that means it is repaired.',
                                                    index) //TODO: index kontrol edilmeli.
                                                ,
                                                //shape: const StadiumBorder(),
                                                child: Text(
                                                  'Delete',
                                                  style: TextStyle(
                                                      fontSize: 8.0,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  maxLines: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )),
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  editReturned(int id, String element) {
    setState(() {
      for (MarkerModel model in markerModels) {
        if (model.id == id) {
          model.label = element;
        }
      }
    });
  }

  jumpToMarkersPage(markerId) {
    int pageNumber = 0;
    for (var item in markerPageMap.keys) {
      if (markerId == markerPageMap[item]) {
        pageNumber = item;
      }
    }
    if (_pageController != null) {
      _pageController.animateToPage(pageNumber,
          duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
    }
  }

  Future getPotholes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    var body = jsonEncode({"status": "NOT-FIXED"});
    var response = await post(baseURL + '/rest/getPotholes/',
        headers: headers, body: body);
    var extractedData = jsonDecode(response.body);
    for (var element in extractedData) {
      MarkerModel model = new MarkerModel.fromJson(element);
      markerModels.add(model);
    }
    setState(() {
      _setMarkers();
    });
  }

  Future getPotholesForFilter() async {
    var headers = {'Content-Type': 'application/json'};
    var body = jsonEncode({'label': selectedElement.toString(), "status": "NOT-FIXED" });
    var response = await post(baseURL + '/rest/getPotholes/',
        headers: headers, body: body);
    print(response.body.toString());
    var extractedData = jsonDecode(response.body);
    for (var element in extractedData) {
      MarkerModel model = new MarkerModel.fromJson(element);
      markerModels.add(model);
    }
    setState(() {
      _setMarkers();
    });
  }

  Future deletePotholes(int potholeId) async {
    var headers = {'Content-Type': 'application/json'};
    var response = await delete(baseURL + '/rest/deletePothole/$potholeId',
        headers: headers);
    print(response.body);
    int code = response.statusCode;
    print(code);
    if (code == 200) {
      deleteMarkerAndPage(potholeId);
    } else {
      //TODO: basarisiz sayfasi bas
    }
  }

  deleteMarkerAndPage(int potholeId) {
    print(potholeId);

    int index = 0;
    for (MarkerModel item in markerModels) {
      if (item.id == potholeId) {
        markerModels.removeAt(index);
        markerPageMap.remove(index);
        break;
      }
      index++;
    }
    index = 0;
    setState(() {
      _markers.clear();
      _setMarkers();
    });
  }

  showTravelledRoads() async {
    List<List<LatLng>> travelledLocCoords = new List<List<LatLng>>();
    String travelledRoads =
        "https://lit-meadow-91580.herokuapp.com/rest/getRoutes/";
    var response = await get(travelledRoads);
    var extractedData = jsonDecode(response.body);
    for (var element in extractedData) {
      List<LatLng> tempCoords = new List<LatLng>();
      for (var points in element["points"]) {
        tempCoords.add(LatLng(points["latitude"], points["longitude"]));
      }
      travelledLocCoords.add(tempCoords);
    }

    for (int i = 0; i < travelledLocCoords.length; i++) {
      for (int j = 0; j < travelledLocCoords[i].length - 1; j++) {
        routeId++;
        await googleMapPolyline.getCoordinatesWithLocation(
            origin: LatLng(travelledLocCoords[i][j].latitude,
                travelledLocCoords[i][j].longitude),
            destination: LatLng(travelledLocCoords[i][j + 1].latitude,
                travelledLocCoords[i][j + 1].longitude),
            mode: RouteMode.driving);
      }
      polyline.add(Polyline(
        polylineId: PolylineId(routeId.toString()),
        visible: true,
        points: travelledLocCoords[i],
        color: Colors.purple[300],
      ));
    }
  }

  _setMarkers() {
    int i = 0;
    markerModels.forEach((element) {
      LatLng coordinates = new LatLng(element.latitude, element.longitude);
      Marker marker = new Marker(
          markerId: MarkerId(element.id
              .toString()), //TODO: Marker id gelmeli ve 0'dan başlamalı.
          position: coordinates,
          onTap: () {
            jumpToMarkersPage(element.id);
          },
          infoWindow: InfoWindow(
            title: element.toString(),
            snippet: element.addr.toString(),
            onTap: () {
              //_saveCoords(element.longitude, element.latitude);
            },
          ),
          icon: BitmapDescriptor.defaultMarker);
      markerPageMap[i] = element.id; //element.id marker id demek.
      _markers.add(marker);
      i++;
    });
  }

  Widget button(Function function, IconData icon) {
    return Container(
        width: 40.0,
        height: 40.0,
        child: FittedBox(
            child: FloatingActionButton(
                heroTag: null,
                onPressed: function,
                materialTapTargetSize: MaterialTapTargetSize.padded,
                backgroundColor: Colors.blue,
                child: Icon(icon, size: 36.0))));
  }

  moveCameraToSearchAdress(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      final GoogleMapController controller = await _controller.future;
      LatLng target = new LatLng(lat, lng);
      await controller.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: target, zoom: 14.0)));

      await getScreenCoordinates(target);
    }
  }

  getScreenCoordinates(LatLng _currentPosition) async {
    Size _screenSize =
        MediaQuery.of(context).size; //width and height of the device in pixels
    GoogleMapController controller = await _controller.future;
    ScreenCoordinate _screenCoordinate = await controller.getScreenCoordinate(
        _currentPosition); //returns the center of the map in screen pixels

    LatLng leftBound = await controller.getLatLng(new ScreenCoordinate(
        x: 0,
        y: _screenCoordinate
            .y)); //LatLng coordinates from screen pixel coordinates
    LatLng rightBound = await controller.getLatLng(new ScreenCoordinate(
        x: _screenSize.width.round(),
        y: _screenCoordinate
            .y)); //LatLng coordinates from screen pixel coordinates

    LatLng topBound = await controller.getLatLng(new ScreenCoordinate(
        x: _screenCoordinate.x,
        y: _screenSize.height
            .round())); //LatLng coordinates from screen pixel coordinates
    LatLng bottomBound = await controller.getLatLng(new ScreenCoordinate(
        x: _screenCoordinate.x,
        y: 0)); //LatLng coordinates from screen pixel coordinates

    /* print("LEFT"); 
    print(leftBound.latitude);
    print(leftBound.longitude);
    print("RIGHT");
    print(rightBound.latitude);
    print(rightBound.longitude);
    print("TOP");
    print(topBound.latitude);
    print(topBound.longitude);
    print("BOT");
    print(bottomBound.latitude);
    print(bottomBound.longitude);*/
  }

  var textController = new TextEditingController();
  Icon actionIcon = new Icon(Icons.search);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        //extendBodyBehindAppBar: true,
        resizeToAvoidBottomPadding: false,
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: drawerWidget(),
                decoration: BoxDecoration(color: Colors.orange[200]),
              ),
              ListTile(
                leading: Icon(Icons.camera),
                title: Text("Detect Potholes!"),
                onTap: () {
                  _onCameraButtonPressed();
                },
              ),
              ListTile(
                leading: Icon(Icons.camera),
                title: Text("Manuel Pothole Detection"),
                onTap: () {
                  _onManuelDetection();
                },
              ),
              ListTile(
                leading: Icon(Icons.control_point),
                title: Text("Control Detected Potholes"),
                onTap: () {
                  _onControl();
                },
              ),
              ListTile(
                leading: Icon(Icons.satellite),
                title: Text("Change Map Type to Satellite"),
                onTap: () {
                  setState(() {
                    if (_currentMapType == MapType.normal) {
                      _currentMapType = MapType.satellite;
                    }
                  });
                },
              ),
              ListTile(
                leading: Icon(Icons.map),
                title: Text("Change Map Type to Normal"),
                onTap: () {
                  setState(() {
                    if (_currentMapType == MapType.satellite) {
                      _currentMapType = MapType.normal;
                    }
                  });
                },
              ),
              ListTile(
                leading: Icon(Icons.refresh),
                title: Text("Refresh The Map"),
                onTap: () {
                  setState(() {
                    refreshMap();
                  });
                },
              ),
              ListTile(
                leading: Icon(Icons.directions),
                title: Text("Assign Routes"),
                onTap: () {
                  _onRoutingButtonPressed();
                },
              ),
              !isAdmin
                  ? ListTile(
                      leading: Icon(Icons.report),
                      title: Text("Current Report"),
                      enabled: ReportPage.reportList != null,
                      onTap: () => Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => ReportPage())))
                  : Container(),
              ListTile(
                  leading: Icon(Icons.report),
                  title: Text("Get Report"),
                  onTap: () => Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => GetReportPage()))),
              /*ListTile(
                  leading: Icon(Icons.notifications),
                  title: Text("Notification"),
                  onTap: () => Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => MessagingWidget()))),*/
              ExpansionTile(
                leading: Icon(Icons.filter_list),
                title: Text("Filter"),
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: createRadioListItems(),
                  ),
                ],
              ),
              ListTile(
                  leading: Icon(Icons.list),
                  title: Text("See the List of Potholes"),
                  onTap: () => Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => PotholeListViewWidget(
                              modelList: markerModels,
                              notifyParent: zoomCameraFromListView)))),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text("Logout!"),
                onTap: () {
                  _onLogoutButtonPressed(context);
                },
              ),
            ],
          ),
        ),
        body: _center == null
            ? Container(
                child: Center(
                  child: Text(
                    'loading map..',
                    style: TextStyle(
                        fontFamily: 'Avenir-Medium', color: Colors.grey[400]),
                  ),
                ),
              )
            : Stack(
                children: <Widget>[
                  GoogleMap(
                    onMapCreated: _onMapCreated,
                    polylines: polyline,
                    myLocationEnabled: true,
                    padding: EdgeInsets.only(
                      top: 100.0,
                    ),
                    initialCameraPosition:
                        CameraPosition(target: _center, zoom: 11.0),
                    mapType: _currentMapType,
                    markers: Set.from(
                        _markers), //TODO: Get isteğinde burası ile işin olacak.
                    onCameraMove: _onCameraMove,
                  ),
                  Positioned(
                    bottom: 60.0,
                    child: Container(
                      height: 125.0,
                      width: MediaQuery.of(context).size.width,
                      child: PageView.builder(
                        controller: _pageController,
                        itemCount: _markers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _markersList(index);
                        },
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: AppBar(
                        centerTitle: true,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(
                            bottom: Radius.circular(20),
                          ),
                        ),
                        backgroundColor: Colors.orange[200],
                        title: appBarWidget(),
                        iconTheme: IconThemeData(color: Colors.black),
                        //elevation: 40.0,
                        actions: <Widget>[
                          new IconButton(
                              icon: actionIcon,
                              onPressed: () async {
                                Prediction p = await PlacesAutocomplete.show(
                                    context: context,
                                    apiKey: kGoogleApiKey,
                                    mode: Mode.fullscreen,
                                    language: "tr",
                                    components: [
                                      new Component(Component.country, "tr")
                                    ]);
                                moveCameraToSearchAdress(p);
                              }),
                        ]),
                  ),
                ],
              ));
  }

  zoomCameraFromListView(id) {
    Navigator.of(context).pop();
    jumpToMarkersPage(id);
    //moveCamera();
  }

  Widget appBarWidget() {
    return Image(
      image: AssetImage("assets/logo.png"),
      height: MediaQuery.of(context).size.height / 15,
      width: MediaQuery.of(context).size.width / 2.65,
      fit: BoxFit.cover,
    );
  }

  Widget drawerWidget() {
    return Image(
      image: AssetImage("assets/drawerLogo.png"),
      height: MediaQuery.of(context).size.height / 22,
      width: MediaQuery.of(context).size.width / 20,
      fit: BoxFit.fitHeight,
    );
  }

  String selectedElement;
  List<Widget> createRadioListItems() {
    print(selectedElement);

    List<Widget> widgets = [];
    final List<String> prior = ['IVEDI', 'YUKSEK', 'ORTA', 'DUSUK'];
    for (String element in prior) {
      widgets.add(
        RadioListTile(
          value: element,
          groupValue: selectedElement,
          title: Text(
            element,
            style: TextStyle(
              color: colorOfImportanceSet[element],
              //fontSize: 20.0,
              //fontWeight: FontWeight.bold,
            ),
          ),
          onChanged: (currentSelection) {
            setSelectedRadio(currentSelection);
          },
          selected: selectedElement != null
              ? selectedElement.compareTo(element) == 0
              : false,
          activeColor: colorOfImportanceSet[element],
        ),
      );
    }
    Widget button = new ButtonBar(
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        new RaisedButton(
          onPressed: selectedElement == null
              ? null
              : () {
                  setState(() {
                    setMarkersForFilter();
                  });
                },
          child: new Text("Apply"),
          color: Colors.green, //TODO: filtreleme baglanacak
        ),
        new RaisedButton(
          onPressed: selectedElement == null
              ? null
              : () {
                  setState(() {
                    selectedElement = null;
                    markerModels.clear();
                    markerPageMap.clear();
                    _markers.clear();
                    getPotholes();
                  });
                },
          child: new Text("Clear"),
          color: Colors.red,
        ),
      ],
    );
    widgets.add(button);
    return widgets;
  }

  setSelectedRadio(String selected) {
    setState(() {
      if (selectedElement != null && selected.compareTo(selectedElement) == 0) {
        selectedElement = null;
      } else {
        selectedElement = selected;
      }
    });
  }

  setMarkersForFilter() {
    markerModels.clear();
    markerPageMap.clear();
    _markers.clear();
    getPotholesForFilter();
  }

  refreshMap() {
    markerModels.clear();
    markerPageMap.clear();
    _markers.clear();
    getPotholes();
  }

  _onDeletePressed(context, id) {
    deletePotholes(markerPageMap[id]);
    Navigator.of(context, rootNavigator: true).pop();
  }

  _onCancelPressed(context) {
    Navigator.of(context, rootNavigator: true).pop();
  }

  confirm(BuildContext context, String title, String description, int id) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext contex) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(description),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => _onDeletePressed(context, id),
                  child: Text('Delete')),
              FlatButton(
                  onPressed: () => _onCancelPressed(context),
                  child: Text('Cancel')),
            ],
          );
        });
  }

  detail(BuildContext context, int pageNumber) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isAdmin = prefs.getBool("isAdmin");
    MarkerModel model;
    for (var item in markerModels) {
      if (item.id == markerPageMap[pageNumber]) {
        model = item;
      }
    }
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext contex) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            backgroundColor: Colors.black12,
            contentPadding: EdgeInsets.all(0.0),
            content: DetailDialogWidget(
                model: model, cameras: cameras, isAdmin: isAdmin),
          );
        });
  }
}
