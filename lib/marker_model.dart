import 'package:roadpulse_flutter/address_model.dart';
import 'package:roadpulse_flutter/image_model.dart';

class MarkerModel {
  int user_id;
  int id;
  final AddressModel addr;
  final String link;
  String label;
  final String creation_date;
  final double latitude;
  final double longitude;

  MarkerModel(
      {this.id,
      this.addr,
      this.link,
      this.label,
      this.creation_date,
      this.latitude,
      this.longitude});

  factory MarkerModel.fromJson(Map<String, dynamic> json) {
    return MarkerModel(
        id: json['id'] as int,
        addr: AddressModel.fromJson(json['addr']),
        link: ImageModel.fromJson(json['image']).link,
        label: json['label'] as String,
        creation_date: json['creation_date'] as String,
        latitude: json['latitude'] as double,
        longitude: json['longitude'] as double);
  }
  @override
  String toString() {
    return label;
  }

  void setUserId(int id) {
    this.user_id = id;
  }

  String formatCreationDate() {
    if(creation_date != null){
      List<String> parsedDate = creation_date.split("-");
      return "Detection Date: " + parsedDate[2].substring(0,parsedDate[2].indexOf("T")) + "/" + parsedDate[1] + "/" + parsedDate[0];
    }
    return "";
  }
    String onlyDate() {
    if(creation_date != null){
      List<String> parsedDate = creation_date.split("-");
      return parsedDate[2].substring(0,parsedDate[2].indexOf("T")) + "/" + parsedDate[1] + "/" + parsedDate[0];
    }
    return "";
  }
}
