import 'package:flutter/material.dart';

import 'marker_model.dart';

class PotholeListViewWidget extends StatefulWidget {
  final List<MarkerModel> modelList;
  final Function(dynamic) notifyParent;
  PotholeListViewWidget(
      {Key key, @required this.modelList, @required this.notifyParent})
      : super(key: key);

  @override
  _PotholeListViewWidget createState() => new _PotholeListViewWidget();
}

class _PotholeListViewWidget extends State<PotholeListViewWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: widget.modelList.length != 0
          ? ListView.builder(
              itemCount: widget.modelList.length,
              itemBuilder: (context, index) {
                return Card(
                  color: Colors.orange[200],
                  child: ListTile(
                    leading: drawTheImage(index),
                    title: Text(widget.modelList[index].addr.toString()),
                    subtitle: Text(widget.modelList[index].label),
                    onTap: () {
                      Navigator.of(context).pop();
                      widget.notifyParent(widget.modelList[index].id);
                    },
                  ),
                );
              })
          : Container(
              child: Center(
                child: Text(
                  'There is no pothole in server.',
                  style: TextStyle(
                      fontFamily: 'Avenir-Medium', color: Colors.grey[400]),
                ),
              ),
            ),
    );
  }

  Widget drawTheImage(index) {
    return Container(
      height: 90.0,
      width: 90.0,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(widget.modelList[index].link),
            fit: BoxFit.cover),
      ),
    );
  }
}
