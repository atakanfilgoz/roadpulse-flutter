import 'package:google_maps_flutter/google_maps_flutter.dart';

class PotholeModel {
  String link;
  String label;
  //String creationDate;
  LatLng coordinates;

  PotholeModel(
      {this.link,
      this.label,
      //this.creationDate,
      this.coordinates});
  @override
  String toString() {
    return label;
  }
}
