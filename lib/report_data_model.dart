
import 'package:roadpulse_flutter/marker_model.dart';

class ReportData{
  String imagePath;
  MarkerModel model;

  ReportData({
    this.imagePath,
    this.model
  });

  @override
  String toString(){
    return model.toString() + ' ' + imagePath;
  }
}