import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:roadpulse_flutter/send_report.dart';
import 'package:roadpulse_flutter/single_report_detail.dart';

import 'report_data_model.dart';

class ReportPage extends StatefulWidget {
  static List<ReportData> reportList;

  bool isListView = true;
  final Map<String, Color> colorOfImportanceSet = {
    'IVEDI': Colors.red,
    'YUKSEK': Colors.orange,
    'ORTA': Colors.yellow[600],
    'DUSUK': Colors.green
  };

  _ReportPage createState() => _ReportPage();
}

class _ReportPage extends State<ReportPage> {
  PageController _controller;

  @override
  void initState() {
    _controller = PageController(initialPage: 0, viewportFraction: 0.9);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(20),
          ),
        ),
        backgroundColor: Colors.orange[200],
        title: Text("Current Report"),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Stack(
        children: <Widget>[
          ListView.builder(
            itemCount: ReportPage.reportList.length,
            itemBuilder: (context, index) {
              return Card(
                  color: Colors.black38,
                  child: Slidable(
                    actionPane: SlidableDrawerActionPane(),
                    actionExtentRatio: 0.25,
                    child: Container(
                        color: Colors.black38,
                        child: ListTile(
                          onTap: () async {
                            await Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => new SingleReportDetail(
                                    reportData: ReportPage.reportList[index])));
                          },
                          title: Text(
                              ReportPage.reportList[index].model.addr
                                  .toString(),
                              style: TextStyle(color: Colors.white)),
                          leading: Image.file(
                              File(ReportPage.reportList[index].imagePath)),
                          subtitle: Text(
                              ReportPage.reportList[index].model.label,
                              style: TextStyle(
                                  color: widget.colorOfImportanceSet[ReportPage
                                      .reportList[index].model.label])),
                        )),
                    secondaryActions: <Widget>[
                      IconSlideAction(
                        caption: 'Delete',
                        color: Colors.red,
                        icon: Icons.delete,
                        onTap: () => _deleteItem(index),
                      ),
                    ],
                  ));
            },
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.check),
          onPressed: () async {
            await _showDialog(context);
            Navigator.of(context).pop();
          }
          /*showDialog(
              context: context,
              builder: (_) {
                return SendReport();
              });*/

          ),
    );
  }

  _deleteItem(index) {
    
    setState(() {
      print(ReportPage.reportList.length);
      
      ReportPage.reportList.removeAt(index);
      if(ReportPage.reportList.length == 0){
        Navigator.of(context).pop();
        ReportPage.reportList = null;
      }
    });
  }

  _showDialog(context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext contex) {
          return SendReport();
        });
  }
}
