import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:roadpulse_flutter/map_routing.dart';
import 'dart:convert';
import 'dart:math' as math;
import 'map_routing.dart';

class AssignRoutesDialog extends StatefulWidget {
  AssignRoutesDialog();

  @override
  AssignRoutesDialogState createState() => new AssignRoutesDialogState();
}

class AssignRoutesDialogState extends State<AssignRoutesDialog> {
  String selectedelementForRoute;
  String dropdownValue = '7';

  @override
  void dispose() {
    resetPath();
    super.dispose();
  }

  List<Widget> createRadioListItemsForRoute() {
    List<Widget> widgets = [];
    final List<String> prior = ['user1', 'user2', 'user3', 'user4'];
    widgets.add(Container(
      child: Text("Days"),
    ));
    widgets.add(DropdownButton<String>(
      value: dropdownValue,
      icon: Icon(Icons.calendar_view_day),
      iconSize: 36,
      elevation: 8,
      style: TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 5,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: <String>['7', '14', '21', '28']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    ));

    for (String element in prior) {
      widgets.add(
        RadioListTile(
          value: element,
          groupValue: selectedelementForRoute,
          title: Text(
            element,
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          onChanged: (currentSelection) {
            setSelectedRadioRouting(currentSelection);
          },
          selected: selectedelementForRoute != null
              ? selectedelementForRoute.compareTo(element) == 0
              : false,
          activeColor: Colors.red[300],
        ),
      );
    }
    return widgets;
  }

  setSelectedRadioRouting(String selected) {
    setState(() {
      if (selectedelementForRoute != null &&
          selected.compareTo(selectedelementForRoute) == 0) {
        selectedelementForRoute = null;
      } else {
        selectedelementForRoute = selected;
      }
    });
  }

  _onAssignPressed(context) async {
    String baseURL =
        "https://lit-meadow-91580.herokuapp.com/rest/assignTask/"; // endpoint gelecek
    String assignedUser = "";
    if (selectedelementForRoute == "user1") {
      assignedUser = "13";
    } else if (selectedelementForRoute == "user2") {
      assignedUser = "16";
    } else if (selectedelementForRoute == "user3") {
      assignedUser = "17";
    } else if (selectedelementForRoute == "user4") {
      assignedUser = "18";
    }

    var now = new DateTime.now();
    var durationTime = now.add(new Duration(days: int.parse(dropdownValue)));
    String formData = durationTime.toString();
    String dateForm = formData.substring(5, 7) +
        "/" +
        formData.substring(8, 10) +
        "/" +
        formData.substring(2, 4) +
        " " +
        formData.substring(11, 13) +
        ":" +
        formData.substring(14, 16) +
        ":" +
        formData.substring(17, 19);
    print("Length");

    print(RoutingMapSampleState.routeCoords.length);
    String data = "{\"assigned_user_id\":" + assignedUser + "," + "\n";
    data += "\"deadline\":" + "\"" + dateForm + "\"" + "," + "\n";
    data += "\"coordinates\": [\n";
    for (int i = 0; i < RoutingMapSampleState.routeCoords.length; i++) {
      data = data + "{\n";
      data = data +
          "\"latitude\":" +
          RoutingMapSampleState.routeCoords[i].latitude.toString() +
          ",\n";
      data = data +
          "\"longitude\":" +
          RoutingMapSampleState.routeCoords[i].longitude.toString() +
          "\n";
      if (RoutingMapSampleState.routeCoords.length - 1 == i) {
        data = data + "}\n],\n";
      } else {
        data = data + "},\n";
      }
    }
    data += "\"potholes\": [\n";

    for (int i = 0; i < RoutingMapSampleState.potholeIds.length; i++) {
      if (i == (RoutingMapSampleState.potholeIds.length - 1)) {
        data += RoutingMapSampleState.potholeIds[i].toString() + "]\n}\n";
      } else {
        data += RoutingMapSampleState.potholeIds[i].toString() + ",";
      }
    }
    RoutingMapSampleState.potholeIds = new List();
    print(data);
    http.Response response = await http.post(baseURL, body: data);
    print(response.statusCode);
    RoutingMapSampleState.routeCoords = new List();
    RoutingMapSampleState.potholeIds = new List();
    Navigator.of(context, rootNavigator: true).pop();
  }

  _onResetButtonPressed(context) {
    resetPath();
    Navigator.of(context, rootNavigator: true).pop();
  }

  void resetPath() {
    RoutingMapSampleState.routeCoords = new List();
    RoutingMapSampleState.polyline = RoutingMapSampleState.polyline
        .difference(RoutingMapSampleState.polylineDiff);
    RoutingMapSampleState.potholeIds = new List();
  }

  _onDeleteButtonPressed(context) async {
    String assignedUser = "";
    if (selectedelementForRoute == "user1") {
      assignedUser = "13";
    } else if (selectedelementForRoute == "user2") {
      assignedUser = "16";
    } else if (selectedelementForRoute == "user3") {
      assignedUser = "17";
    } else if (selectedelementForRoute == "user4") {
      assignedUser = "18";
    }
    String baseURL = "https://lit-meadow-91580.herokuapp.com/rest/deleteTask/" +
        assignedUser;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    var response = await http.delete(baseURL, headers: headers);
    print(response.body);
    Navigator.of(context, rootNavigator: true).pop();
  }

  void _autoRouting(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    String baseURL = "https://lit-meadow-91580.herokuapp.com"; //
    var response = await http.get(baseURL + '/rest/getTask/', headers: headers);
    var extractedData = jsonDecode(response.body);
    String assignedUser = "";
    if (extractedData[0]["potholes"].length == 0) {
      assignedUser = "13";
    } else if (extractedData[1]["potholes"].length == 0) {
      assignedUser = "16";
    } else if (extractedData[2]["potholes"].length == 0) {
      assignedUser = "17";
    } else if (extractedData[3]["potholes"].length == 0) {
      assignedUser = "18";
    } else {
      int min = extractedData[0]["potholes"].length;
      int index = 0;
      for (int i = 1; i < 4; i++) {
        if (min > extractedData[i]["potholes"].length) {
          min = extractedData[i]["potholes"].length;
          index = i;
        }
      }
      if (index == 0) {
        assignedUser = "13";
      } else if (index == 1) {
        assignedUser = "16";
      } else if (index == 2) {
        assignedUser = "17";
      } else if (index == 3) {
        assignedUser = "18";
      }
    }
    print(assignedUser);
    var now = new DateTime.now();
    var durationTime = now.add(new Duration(days: 7));
    String formData = durationTime.toString();
    String dateForm = formData.substring(5, 7) +
        "/" +
        formData.substring(8, 10) +
        "/" +
        formData.substring(2, 4) +
        " " +
        formData.substring(11, 13) +
        ":" +
        formData.substring(14, 16) +
        ":" +
        formData.substring(17, 19);
    String data = "{\"assigned_user_id\":" + assignedUser + "," + "\n";
    data += "\"deadline\":" + "\"" + dateForm + "\"" + "," + "\n";
    data += "\"coordinates\": [\n";
    for (int i = 0; i < RoutingMapSampleState.routeCoords.length; i++) {
      data = data + "{\n";
      data = data +
          "\"latitude\":" +
          RoutingMapSampleState.routeCoords[i].latitude.toString() +
          ",\n";
      data = data +
          "\"longitude\":" +
          RoutingMapSampleState.routeCoords[i].longitude.toString() +
          "\n";
      if (RoutingMapSampleState.routeCoords.length - 1 == i) {
        data = data + "}\n],\n";
      } else {
        data = data + "},\n";
      }
    }
    data += "\"potholes\": [\n";

    for (int i = 0; i < RoutingMapSampleState.potholeIds.length; i++) {
      if (i == (RoutingMapSampleState.potholeIds.length - 1)) {
        data += RoutingMapSampleState.potholeIds[i].toString() + "]\n}\n";
      } else {
        data += RoutingMapSampleState.potholeIds[i].toString() + ",";
      }
    }
    RoutingMapSampleState.potholeIds = new List();
    print(data);
    http.Response response2 =
        await http.post(baseURL + "/rest/assignTask/", body: data);
    print(response2.statusCode);
    RoutingMapSampleState.routeCoords = new List();
    RoutingMapSampleState.potholeIds = new List();
    Navigator.of(context, rootNavigator: true).pop();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      backgroundColor: Colors.white,
      title: Text('Assing Route the Selected User'),
      content: SingleChildScrollView(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: createRadioListItemsForRoute(),
      )),
      actions: <Widget>[
        Row(children: <Widget>[
          FlatButton(
              onPressed: () => _onAssignPressed(context),
              child: Text(
                'Assign',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              )),
          FlatButton(
              onPressed: () => _onResetButtonPressed(context),
              child: Text(
                'Reset',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              )),
          FlatButton(
              onPressed: () => _onDeleteButtonPressed(context),
              child: Text(
                'Delete',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              )),
          FlatButton(
              onPressed: () => _autoRouting(context),
              child: Text(
                'Auto',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ))
        ]),
      ],
    );
  }
}
