import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SendFeedbackAnswer extends StatefulWidget {
  Map<int, bool> potholeMapping;
  int report_id;

  SendFeedbackAnswer({Key key, this.potholeMapping, this.report_id})
      : super(key: key);

  _SendFeedbackAnswer createState() => _SendFeedbackAnswer();
}

class _SendFeedbackAnswer extends State<SendFeedbackAnswer> {
  TextEditingController noteController;

  @override
  void initState() {
    super.initState();
    noteController = new TextEditingController();
  }

  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text("Add Note to Your Reply"),
      content: SingleChildScrollView(
          child: ListBody(children: <Widget>[
        Text(
            "To give explanation to repairman for your reply please add note."),
        GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Column(children: [
              Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: TextField(
                      controller: noteController,
                      textInputAction: TextInputAction.done,
                      decoration: new InputDecoration(
                        hintText: "Enter a Reply Note",
                        labelText: "Note",
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: 10,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ))),
            ]))
      ])),
      actions: <Widget>[
        RaisedButton(
          onPressed: () => _sendResultFromFeedback(),
          child: Text("Send Reply", style: TextStyle(color: Colors.white)),
          color: Colors.lightGreen[800],
          shape: const StadiumBorder(),
        ),
        SizedBox(
          width: 15.0,
        ),
        RaisedButton(
          onPressed: _onClearPressed,
          color: Colors.redAccent[700],
          child:
              Text("Clear the Fields", style: TextStyle(color: Colors.white)),
          shape: const StadiumBorder(),
        )
      ],
    );
  }

  _onClearPressed() {
    noteController.clear();
  }

  _sendResultFromFeedback() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    List<int> fixedOnes = new List();
    List<int> notFixedOnes = new List();
    String status = "PASSED";
    //String title = titleController.text;
    String note = noteController.text;

    for (var element in widget.potholeMapping.keys) {
      if (widget.potholeMapping[element] == false) {
        status = "DENIED";
        notFixedOnes.add(element);
      } else {
        fixedOnes.add(element);
      }
    }

    var body = jsonEncode({
      "text": note,
      "reply_status": status,
      "validated_pothole_ids": fixedOnes,
      "denied_pothole_ids": notFixedOnes,
      "replied_report_id": widget.report_id,
    });
    print(body);
    var response =
        await post('https://lit-meadow-91580.herokuapp.com/rest/replyReport/', headers: headers, body: body);
    var extractedData = jsonDecode(response.body);
    if (response.statusCode == 200) {
      print(extractedData.toString());
      Navigator.of(context).pop();
    }
  }
}
