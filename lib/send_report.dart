import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'report_data_model.dart';
import 'report_screen.dart';

class SendReport extends StatefulWidget {
  static List<ReportData> reportList;

  _SendReport createState() => _SendReport();
}

class _SendReport extends State<SendReport> {
  TextEditingController noteController;
  TextEditingController titleController;

  @override
  void initState() {
    super.initState();
    noteController = new TextEditingController();
    titleController = new TextEditingController();
  }

  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text("Add Title and Note to Your Report"),
      content: SingleChildScrollView(
          child: ListBody(children: <Widget>[
        Text("To give explanation to admin for your report please add note."),
        GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Column(children: [
              Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: TextField(
                      controller: titleController,
                      textInputAction: TextInputAction.done,
                      decoration: new InputDecoration(
                        hintText: "Enter a Title",
                        labelText: "Title",
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ))),
              Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: TextField(
                      controller: noteController,
                      textInputAction: TextInputAction.done,
                      decoration: new InputDecoration(
                        hintText: "Enter a Note",
                        labelText: "Note",
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: 10,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ))),
              /*Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    RaisedButton(
                      onPressed: _sendReport(),
                      child:
                          Text("Send Report", style: TextStyle(color: Colors.white)),
                      color: Colors.lightGreen[800],
                      shape: const StadiumBorder(),
                    ),
                    SizedBox(
                      width: 15.0,
                    ),
                    RaisedButton(
                      onPressed: _onClearPressed,
                      color: Colors.redAccent[700],
                      child:
                          Text("Clear the Fields", style: TextStyle(color: Colors.white)),
                      shape: const StadiumBorder(),
                    )
                  ],
                )*/
            ]))
      ])),
      actions: <Widget>[
        RaisedButton(
          onPressed: () => _sendReport(),
          child: Text("Send Report", style: TextStyle(color: Colors.white)),
          color: Colors.lightGreen[800],
          shape: const StadiumBorder(),
        ),
        SizedBox(
          width: 15.0,
        ),
        RaisedButton(
          onPressed: _onClearPressed,
          color: Colors.redAccent[700],
          child:
              Text("Clear the Fields", style: TextStyle(color: Colors.white)),
          shape: const StadiumBorder(),
        )
      ],
    );
  }

  _sendReport() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    String title = titleController.text;
    String note = noteController.text;

    Map reportData = {"title": title, "text": note};
    List imageMapping = new List();

    for (int i = 0; i < ReportPage.reportList.length; i++) {
      int x = i + 1;
      List data = ["img_file" + x.toString()];
      Map pothole = {"id": ReportPage.reportList[i].model.id, "images": data};
      imageMapping.add(pothole);
    }
    print(imageMapping.toString());

    var uri =
        Uri.parse('https://lit-meadow-91580.herokuapp.com/rest/sendReport/');
    var request = http.MultipartRequest('POST', uri);
    request.headers.addAll(headers);
    print(request.fields["report"]);

    for (int i = 0; i < ReportPage.reportList.length; i++) {
      int x = i + 1;

      request.files.add(new http.MultipartFile.fromBytes(
          'img_file' + x.toString(),
          await File.fromUri(Uri.parse(ReportPage.reportList[i].imagePath))
              .readAsBytes(),
          contentType: MediaType('image', 'png'),
          filename: 'a.png'));
    }
    request.fields["report"] = json.encode(reportData);
    request.fields["image_mapping"] = json.encode(imageMapping);
    var response = await request.send();

    if (response.statusCode == 200) {
      Navigator.of(context).pop(reportData);
      ReportPage.reportList = null;
      print('Uploaded!');
    } else {
      print(response.request.headers.toString());
      print(response.statusCode);
    }

    /*request.files.add(await http.MultipartFile.fromBytes(
        'img_file', convertImagetoPng(img),
        contentType: MediaType('image', 'png'), filename: 'a.png'));

    var response = await request.send();
    if (response.statusCode == 200) {
      print('Uploaded!');
    }*/
  }

  _onClearPressed() {
    noteController.clear();
    titleController.clear();
  }
}
