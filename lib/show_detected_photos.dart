import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'accept_dialog.dart';

class ShowDetected extends StatefulWidget {
  @override
  ShowDetectedState createState() => new ShowDetectedState();
}

class ShowDetectedState extends State<ShowDetected> {
  String link = "";
  Timer timer;
  static bool setTimer = false;
  static int potholeId;

  @override
  void initState() {
    super.initState();
    timer =
        Timer.periodic(Duration(seconds: 7), (Timer t) => checkForNewPhoto());

  }

  void checkForNewPhoto() async {
    if (setTimer) {
      return;
    }
    print("İstek atıldı.");
    String baseURL = 'https://lit-meadow-91580.herokuapp.com';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var sessionId = prefs.getString('sessionId');
    Map<String, String> headers = {"cookie": sessionId};
    var response = await http.get(baseURL + '/rest/getDetectedPotholes/',
        headers: headers);
    print(response.statusCode);
    setState(() {
      if (response.statusCode == 200) {
        var extractedData = jsonDecode(response.body);
        link = extractedData["image"]["link"];
        potholeId = extractedData["id"];
        print(link);
      } else {
        link = "";
      }
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void deletePhoto() async {
    final String baseURL = 'https://lit-meadow-91580.herokuapp.com';
    var headers = {'Content-Type': 'application/json'};
    var response = await http.delete(baseURL + '/rest/deletePothole/$potholeId',
        headers: headers);
    print(response.body);
    int code = response.statusCode;
    print(code);
    if (code == 200) {
      print("Silindi.");
    } else {
      print("Silinemedi.");
    }
  }

  @override
  Widget build(BuildContext context) {
    var title = 'Show Detected Potholes - Operator Man';

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.orange[200],
            title: Text(title),
          ),
          body: link == ""
              ? Container(
                  child: Text("Waiting for detected potholes..."),
                )
              : Image.network(
                  link,
                ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FloatingActionButton(
                  backgroundColor: link == "" ? Colors.grey : Colors.green,
                  heroTag: null,
                  onPressed: link == ""
                      ? null
                      : () {
                    setTimer = true;
                          showDialog(
                              context: context,
                              builder: (_) {
                                return AcceptDialog();
                              });
                        },
                  child: Icon(Icons.thumb_up),
                ),
                FloatingActionButton(
                  backgroundColor: link == "" ? Colors.grey : Colors.red,
                  heroTag: null,
                  onPressed: link == "" ? null : deletePhoto,
                  child: Icon(Icons.thumb_down),
                )
              ],
            ),
          )),
    );
  }
}
