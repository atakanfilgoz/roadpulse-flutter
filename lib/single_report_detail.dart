import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import 'report_data_model.dart';

class SingleReportDetail extends StatefulWidget {
  final ReportData reportData;

  const SingleReportDetail({Key key, this.reportData}) : super(key: key);

  _SingleReportDetail createState() => _SingleReportDetail();
}

class _SingleReportDetail extends State<SingleReportDetail> {
  final Map<String, Color> colorOfImportanceSet = {
    'IVEDI': Colors.red,
    'YUKSEK': Colors.orange,
    'ORTA': Colors.yellow[600],
    'DUSUK': Colors.green
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(20),
            ),
          ),
          backgroundColor: Colors.orange[200],
          title: Text("Details of Pothole"),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
            child: Stack(children: <Widget>[
          Column(
            //mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                  margin: EdgeInsets.all(10),
                  color: Colors.black38,
                  child: ListTile(
                      title: titleWidget(), subtitle: subTitleWidget())),
              Text("New Photo of Pothole",
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 20,
                      fontWeight: FontWeight.w900,
                      decoration: TextDecoration.underline)),
              imageWidget(
                  Image.file(File(widget.reportData.imagePath)), context, FileImage(File(widget.reportData.imagePath))),
              Text("Old Photo of Pothole",
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 20,
                      fontWeight: FontWeight.w900,
                      decoration: TextDecoration.underline)),
              imageWidget(Image.network(widget.reportData.model.link), context, NetworkImage(widget.reportData.model.link))
            ],
          )
        ])));
  }
  //child: Image.file(File(widget.reportData.imagePath)),

  Widget subTitleWidget() {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text("Detection Date:",
              style: TextStyle(
                  color: Colors.white60, decoration: TextDecoration.underline)),
          Text(widget.reportData.model.onlyDate(),
              style: TextStyle(color: Colors.white)),
          Text("Adress of Pothole:",
              style: TextStyle(
                  color: Colors.white60, decoration: TextDecoration.underline)),
          Text(widget.reportData.model.addr.toString(),
              style: TextStyle(color: Colors.white))
        ]);
  }

  Widget titleWidget() {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text("Importance Level:",
              style: TextStyle(
                color: Colors.white60,
                decoration: TextDecoration.underline,
              )),
          Text(widget.reportData.model.label,
              style: TextStyle(
                  color: colorOfImportanceSet[widget.reportData.model.label]))
        ]);
  }

  Widget imageWidget(childImage, context, providers) {
    return GestureDetector(
        onTap: () => _biggerPhoto(context, providers),
        child: Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.brown[100],
                boxShadow: [
                  BoxShadow(
                      color: Colors.black87,
                      offset: Offset(0.0, 4.0),
                      blurRadius: 10.0),
                ]),
            child: childImage));
  }

  _biggerPhoto(context, imageWithProvider) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext contex) {
          return Dialog(
              child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width * 2,
            child: ClipRect(
              child: PhotoView(
                tightMode: true,
                imageProvider: imageWithProvider,
                maxScale: PhotoViewComputedScale.covered * 2.0,
                minScale: PhotoViewComputedScale.contained * 1,
                initialScale: PhotoViewComputedScale.contained,
              ),
            ),
          ));
        });
  }
}
